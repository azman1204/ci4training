<?php
namespace App\Models;
use CodeIgniter\Model;

class UserModel extends Model {
    protected $table = 'user';
    protected $returnType = 'object';
    protected $allowedFields = ['user_id', 'name', 'email', 'password'];
    protected $primaryKey = 'id';
}