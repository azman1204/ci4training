<?= $this->extend('inspinia/layout') ?>
<?= $this->section('main') ?>

<div class="ibox"></div>
    <div class="ibox-content row">
        <div class="col-md-3">
            <div class="statistic-box">
            <h4>
                Statistik Jualan 
            </h4>
            <p>
                Jum jualan popular bulan ini
            </p>
                <div class="row text-center">
                    <div class="col-lg-6">
                        <canvas id="doughnutChart" width="100" height="100" style="margin: 18px auto 0"></canvas>
                        <h5>Jenama</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<!-- ChartJS-->
<script src="<?= base_url() ?>/inspinia/js/plugins/chartJs/Chart.min.js"></script>
<script>
$(document).ready(function() {
    var doughnutData = {
        labels: ["App", "Software", "Laptop"],
        datasets: [{
            data: [300, 50, 100],
            backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
        }]
    };

    var doughnutOptions = {
        responsive: false,
        legend: {
            display: false
        }
    };

    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
    new Chart(ctx4, {
        type: 'doughnut',
        data: doughnutData,
        options: doughnutOptions
    });

});
</script>
<?= $this->endSection() ?>