<?= $this->extend('inspinia/layout') ?>
<?= $this->section('main') ?>

<?php if(isset($validator)) : ?>
    <div class="alert alert-danger"><?= $validator->listErrors() ?></div>
<?php endif ?>

<form action="/upload/save" method="post" enctype="multipart/form-data">
    <?= csrf_field() ?>
    Document : <input type="file" name="doc">
    <input type="submit" value="Simpan" class="btn btn-primary">
</form>

<?= $this->endSection() ?>