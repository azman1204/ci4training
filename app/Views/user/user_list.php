<?= $this->extend('inspinia/layout') ?>
<?= $this->section('main') ?>

<form action="<?= base_url() ?>/user/list" method="post">
    <div class="ibox col-md-6">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-2">Name</div>
                <div class="col-md-8"><input type="text" name="name" class="form-control"></div>
                <div class="col-md-2"><input type="submit" value="Search" class="btn btn-primary"></div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">Email</div>
                <div class="col-md-8"><input type="text" name="email" class="form-control"></div>
            </div>
        </div>
    </div>
</form>

<div class="ibox">
    <div class="ibox-content">
        <a href="<?= base_url() ?>/user/new" class="btn btn-primary btn-sm">+ Tambah Pengguna</a>
        <table class="table tale-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= esc($user->name) ?></td>
                        <td><?= $user->email ?></td>
                        <td><?= $user->user_id ?></td>
                        <td>
                            <a href="<?= base_url() ?>/user/edit/<?= $user->id ?>"><i class="fa fa-pencil"></i></a>
                            <a href="<?= base_url() ?>/user/delete/<?= $user->id ?>" onclick="return confirm('Anda Pasti ?')">
                                <i class="fa fa-trash-o text-danger"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <?= $pager->links() ?>
    </div>
</div>
<?= $this->endSection() ?>