<?= $this->extend('inspinia/layout') ?>
<?= $this->section('main') ?>

<div class="ibox ">
    <div class="ibox-title">
        <h5>Maklumat Pengguna</h5>
    </div>
    <div class="ibox-content">

        <?php if (isset($validator)): ?>
            <div class="alert alert-danger"><?= $validator->listErrors() ?></div>
        <?php endif ?>

        <form method="post" action="<?= base_url() ?>/user/save">
            <?= csrf_field() ?>
            <input type="hidden" name="id" value="<?= $data['id'] ?>">
            
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Id Pengguna</label>
                <div class="col-lg-10">
                    <input name="user_id" value="<?= $data['user_id'] ?>" type="text" placeholder="ID Pengguna" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Emel</label>
                <div class="col-lg-10">
                    <input name="email" value="<?= $data['email'] ?>" type="email" placeholder="Emel" class="form-control"> 
                </div>
            </div>

            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Nama</label>
                <div class="col-lg-10"><input name="name" value="<?= $data['name'] ?>" type="text" placeholder="Nama" class="form-control"></div>
            </div>

            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Password</label>
                <div class="col-lg-10"><input name="password" value="<?= $data['password'] ?>" type="password" placeholder="Password" class="form-control"></div>
            </div>

            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?= $this->endSection() ?>