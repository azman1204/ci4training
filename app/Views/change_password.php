
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INSPINIA | Forgot password</title>
    <link href="<?= base_url() ?>/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?= base_url() ?>/inspinia/css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <h2 class="font-bold">Change password</h2>
                    <p>
                        Enter your new password
                    </p>
					
					<?php if (session()->has('err')) : ?>
						<div class="alert alert-danger"><?= session()->get('err') ?></div>
					<?php endif ?>

                    <div class="row">
                        <div class="col-lg-12">
                            
                            <?php if(isset($validator)) : ?>
                                <div class="alert alert-danger"><?= $validator->listErrors() ?></div>
                            <?php endif ?>

                            <form class="m-t" role="form" action="<?= base_url() ?>/update-password" method="post">
                                <input type="hidden" name="token" value="<?= $token ?>">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="New Password" required="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password" required="">
                                </div>
                                <button type="submit" class="btn btn-primary block full-width m-b">Update password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

</body>

</html>
