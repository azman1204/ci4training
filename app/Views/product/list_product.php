<?= $this->extend('inspinia/layout') ?>
<?= $this->section('main') ?>

<div class="ibox">
    <div class="ibox-content">

        <a href="<?= base_url('product/pdf') ?>" class="btn btn-primary btn-sm">Download as PDF</a>
        <a href="<?= base_url('product/sms') ?>" class="btn btn-primary btn-sm">Send SMS</a>

        <?= $this->include('product/_product') ?>
    </div>
</div>

<?= $this->endSection() ?>