<table class="table tale-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rows as $row) : ?>
            <tr>
                <td></td>
                <td><?= $row->name ?></td>
                <td><?= $row->description ?></td>
                <td><?= $row->price ?></td>
                <td></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

<?php if (isset($print)) : ?>
    <style>
        td, th {
            border: 1px solid #999;
        }

        table {
            width: 100%;
        }
    </style>
<?php endif ?>