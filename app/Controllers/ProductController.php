<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProductModel;
use Dompdf\Dompdf;

class ProductController extends BaseController
{
    function index()
    {
        // query data dari table products
        $product = model(ProductModel::class);
        // kaedah alternatif
        //$product = new ProductModel();
        $rows = $product->findAll(); // return data dlm bentuk array
        //d($rows); // dump
        return view('product/list_product', ['rows' => $rows]);
    }

    // jana pdf
    function pdf() {
        $product = model(ProductModel::class);
        $rows = $product->findAll();
        $dompdf = new Dompdf();
        $html = view('product/_product', ['rows' => $rows, 'print' => true]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

    // gitlab.com/azman1204/ci4training
    function sms() {
        $message = urlencode("<a href='https://www.bharian.com.my'>Hello World...</a>");
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $username = urlencode('azman1204');
        $password = urlencode('azman1204');
        $phone_no = urlencode('60162370394');
        $type = 1;
        $sendid = '66300';
        $url = 'https://www.isms.com.my/isms_send.php';
        $url .= "?un=$username&pwd=$password&dstno=$phone_no&msg=$message&type=$type&sendid=$sendid&agreedterm=YES";
        $result = $this->sendSms($url);
    }

    function sendSms($url) {
        $http = curl_init($url);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        return $http_result;
    }
}
