<?php
namespace App\Controllers;

class UploadController extends BaseController {
    // show form to upload file
    function form() {
        return view('upload');
    }

    // save uploaded file
    function save() {
        $rules = [
            'doc' => [
                'label'  => 'Dokumen',
                'rules'  => 'uploaded[doc]|mime_in[doc,image/jpg,image/jpeg,image/gif,image/png]',
                'errors' => [
                    'uploaded' => 'Dokumen wajib dipilih',
                    'mime_in'  => 'Hanya dokumen jenis jpg, jpeg, gif dan png shj dibenarkan'
                ]
            ]
        ];
        
        $ok = $this->validate($rules);
        
        if ($ok) {
            // validation ok
            $file = $this->request->getFile('doc');
            //$file->store(); // save doc. default path = writable/upload/<Y-m-d>/<nama file>
            $file->move("C:\\old", "abc.".$file->getClientExtension());
        } else {
            // validation x ok
            return view('upload', ['validator' => $this->validator]);
        }
    }

    function pieChart() {
        return view('piechart');
    }
}