<?php
namespace App\Controllers;
use App\Models\UserModel;

class AuthController extends BaseController {
    public $db; // properties

    function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function index() {
        return view('login');
    }

    // terima data dari form login, dan buat auth
    function login() {
        $user_id = $this->request->getVar('user_id'); // baca data dari form
        $model = new UserModel();
        $user = $model->where('user_id', $user_id)->first(); // return an object
        if ($user) {
            // user wujud, check password
            $password = $this->request->getVar('password'); // dari form
            
            if (password_verify($password, $user->password)) {
                // user sah!, benarkan masuk
                session()->set(['isLoggedin' => true, 'user_id' => $user_id]);
                return redirect()->to('product/list');
            } else {
                // password x betul
                return redirect()->to('login')->with('err', 'Katalaluan salah');
            }

        } else {
            // user x wujud
            return redirect()->to('login')->with('err', 'Pengguna tidak wujud');
        }
    }

    function logout() {
        // clear all session
        session()->destroy();
        return redirect()->to('login');
    }

    // ada kes dtg ke function ini. 1. bila klik link forgot password, 2. submit form forgot password
    function forgotPassword() {
        if ($this->request->getMethod() == 'get') {
            // klik pd link
            return view('forgot_password');
        } else {
            // submit form
            //$db = \Config\Database::connect();
            // getRowArray()/ getRow() - utk 1 rekod, getResult() - utk banyak rekod
            $user = $this->db->table('user')->where('email', $_POST['email'])->get()->getRow();
            //dd($user);
            if ($user) {
                // email jumpa, return 1 rekod
                // 1. send email, 2. redirect ke login page dgn msg
                helper('text'); // load helper text
                $email = \Config\Services::email();
                $email->setTo($user->email);
                $email->setFrom('admin@ci4training.test');
                $email->setSubject('Tukar kata laluan');
                $token = random_string('alnum', 50);
                $url = base_url().'/change-password/'.$token;
                $email->setMessage("Sila klik link berikut untuk menukar katalaluan. <a href='$url'>Klik</a>");
                $email->send();

                // update token ke table user
                $this->db->table('user')->set('token', $token)->where('user_id', $user->user_id)->update();

                return redirect()->to('login')->with('msg', 'Sila semak emel anda untuk tukar katalaluan');
            } else {
                // email x jumpa
                return redirect()->to('forgot-password')->with('err', 'Emel tidak wujud!');
            }
        }
    }

    function changePassword($token) {
        //echo $token;
        // query ke table user, check token exist or not
        // jika ada, show form change pwd, else, print invalid token
        $user = $this->db->table('user')->where('token', $token)->get()->getRow();
        
        if ($user) {
            // token valid, show page utk change pwd
            return view('change_password', ['token' => $token]);
        } else{
            // token x valid
            echo "Invalid Token!";
        }
    }

    function updatePassword() {
        $user = $this->db->table('user')->where('token', $_POST['token'])->get()->getRow();

        if ($user) {
            // token valid
            $ok = $this->validate([
                'password' => "required|min_length[6]|matches[password_confirm]",
            ]);

            if ($ok) {
                // update password
                $pwd = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $this->db->table('user')
                ->set('password', $pwd)
                ->set('token', '')
                ->where('user_id', $user->user_id)
                ->update();
                return redirect()->to('login')->with('msg', 'Tukar katalaluan berjaya');
            } else {
                // validation tak ok
                return view('change_password', ['validator' => $this->validator, 'token' => $_POST['token']]);
            }
            
        } else {
            // invalid token
            echo 'invalid token!';
        }
    }
}