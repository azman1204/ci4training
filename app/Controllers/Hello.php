<?php
namespace App\Controllers;

class Hello extends BaseController {
    // ci4training.test/hello/world
    function world() {
        return view('hello_world', ['nama' => 'Azman Zakaria']);
    }
}