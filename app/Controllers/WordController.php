<?php
namespace App\Controllers;
use App\Models\UserModel;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class WordController extends BaseController
{

    // jana file msword dari tiada
    public function generate()
    {
        // Creating the new document...
        $phpWord = new PhpWord();
        /* Note: any element you append to a document must reside inside of a Section. */
        // Adding an empty Section to the document...
        $section = $phpWord->addSection();
        // Adding Text element to the Section having font styled by default...
        $section->addText('Hello World...');
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('C:\\old\\helloWorld.docx');
    }

    // jana msworddari template sediada
    function wordWithVariable() {
        $userModel = new UserModel();
        $users = $userModel->findAll();
        foreach ($users as $user) {
            $templateProcessor = new TemplateProcessor('C:\\old\\sample.docx');
            $templateProcessor->setValue('nama', $user->name);
            $templateProcessor->saveAs("C:\\old\\{$user->name}.docx");
        }
        
    }

     // jana msworddari template sediada
     function wordWithVariable2() {
        $userModel = new UserModel();
        $users = $userModel->findAll();
        $values = [];
        
        foreach($users as $user) {
            $arr = ['id' => $user->id, 'name' => $user->name, 'email' => $user->email ];
            $values[] = $arr;
        }

        $templateProcessor = new TemplateProcessor('sample2.docx');
        $templateProcessor->cloneRowAndSetValues('id', $values);
        $templateProcessor->saveAs("docs/sample2_edited.docx");
        return redirect()->to('docs/sample2_edited.docx');
    }
}
