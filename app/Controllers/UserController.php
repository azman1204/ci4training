<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;

class UserController extends BaseController
{
    // user/list?page=1
    function index()
    {
        $rowPerPage = 3;
        $page = $this->request->getVar('page') ?? 1; // $_GET['page']
        $no = ($page - 1) * $rowPerPage + 1;
        $userModel = new UserModel();
        //$users = $userModel->findAll();
        $query = $userModel->where('id is not', null); // return query object

        if (isset($_POST['name'])) {
            // user submit btn search
            if(! empty($_POST['name'])) {
                // carian by name
                $query = $query->like('name', $_POST['name']);
            }

            if(! empty($_POST['email'])) {
                // carian by email
                $query = $query->like('email', $_POST['email']);
            }
        }

        $users = $query->paginate($rowPerPage);
        
        // if (isset($_POST['name'])) {
        //     // user submit btn search
        //     $name = $_POST['name'];
        //     $users = $userModel->like('name', $name)->paginate($rowPerPage);
        // } else {
        //     // klik pd link atau type pd URL
        //     $users = $userModel->paginate($rowPerPage);
        // }
        
        return view('user/user_list', ['users' => $users, 'pager' => $userModel->pager, 'no' => $no]);
    }

    // show user form
    function create()
    {
        $data = [
            'id'       => '',
            'name'     => '',
            'password' => '',
            'email'    => '',
            'user_id'  => '' 
        ];
        return view('user/form', ['data' =>$data]);
    }

    // simpan data
    function save()
    {
        // validation
        $ok = $this->validate([
            'name'     => 'required',
            'email'    => 'valid_email',
            'password' => 'min_length[6]',
            'user_id'  => 'exact_length[12]'
        ]);

        if ($ok) {
            // validation ok
            $userModel = new UserModel();

            if ($_POST['password'] !== 'xxxxxxxx') {
                $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            } else {
                unset($_POST['password']); // unset() remove suatu data dr array
            }
                
            $userModel->save($_POST); // insert dan update, di bezakan by value id
            return redirect()->to('user/list');
        } else {
            // validation ko
            $data = [
                'validator' => $this->validator,
                'data' => $_POST
            ]; // guna utk papar err msg
            return view('user/form', $data);
        }
    }

    function delete($id) {
        $userModel = new UserModel();
        $userModel->delete($id);
        return redirect()->to('user/list')->with('msg', 'Data telah berjaya dihapuskan');
    }

    function edit($id) {
        $userModel = new UserModel();
        $data = (array)$userModel->find($id);
        $data['password'] = 'xxxxxxxx';
        return view('user/form', ['data' => $data]);
    }
}
