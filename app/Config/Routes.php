<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false); // true = mcm ci3

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// jana msword
$routes->get('word/generate', 'WordController::generate');
$routes->get('word/generate-with-var', 'WordController::wordWithVariable');
$routes->get('word/generate-with-var2', 'WordController::wordWithVariable2');

$routes->add('forgot-password', 'AuthController::forgotPassword');
$routes->get('change-password/(:alphanum)', 'AuthController::changePassword/$1');
$routes->post('update-password', 'AuthController::updatePassword');

$routes->get('login', 'AuthController::index');
$routes->post('auth', 'AuthController::login');
$routes->get('/', 'AuthController::index');

$routes->get('hello', function() {
    d(session()->get());
});

// manually generate hash password
$routes->get('password-hash', function() {
    return password_hash('1234', PASSWORD_DEFAULT);
});

// ci4training.test/hello.html
$routes->get('hello.html', 'Hello::world');
$routes->get('user', 'UserController::index');
$routes->get('logout', 'AuthController::logout');

// ---------------- PRODUCT -----------------
$routes->group('product', ['filter' => 'pak-guard'], function($routes) {
    $routes->get('list', 'ProductController::index');
    $routes->get('pdf', 'ProductController::pdf');
    $routes->get('sms', 'ProductController::sms');
});


// ---------------- USER -----------------
// URL = /user/list, /user/new
$routes->group('user', ['filter' => 'pak-guard'], function($routes) {
    $routes->add('list', 'UserController::index'); // add() support GET dan POST
    $routes->get('new', 'UserController::create');
    $routes->post('save', 'UserController::save');
    $routes->get('delete/(:num)', 'UserController::delete/$1');
    // /user/edit/4
    $routes->get('edit/(:num)', 'UserController::edit/$1');
});

// ---------------- UPLOAD -----------------
$routes->group('upload', ['filter' => 'pak-guard'], function($routes) {
    $routes->get('form', 'UploadController::form');
    $routes->post('save', 'UploadController::save');
    $routes->get('flot', 'UploadController::pieChart');
});


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
